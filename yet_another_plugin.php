<?php
/**
 * Plugin Name: Yet Another Plugin
 * Author: Mehedee Hasan
 * Author URI: http://mehedee.me
 * Plugin URI: http://mehedee.me
 * Version: 0.0.1
 * Description: This Plugin will move post to Private automatically after 90 days, if not given a specific date. Expire Date can be set in "Y-m-d" format, e.g. 2000-01-01
 */

add_filter('the_content','change_post_status');
add_action( 'post_submitbox_misc_actions', 'date_to_expire' );
add_action( 'save_post', 'save_date_to_expire' );


function change_post_status($content)
{
    $current_post_id= get_the_ID(); // Retrieving the ID of current Post
    $expire_date_exists=get_post_meta($current_post_id, '_date_to_expire', true);

    if(empty($expire_date_exists))
    {
        return $content;
    }

    else
    {
        $current_time=time(); //Current Time:
        $expire_time=strtotime($expire_date_exists);//Expire Date in Time:

        //Actions;
        if($current_time<$expire_time)
        {
            $msg="This is a limited time offer, The offer will end in X Days."; //  to be implemented soon.
            return $msg . $content;
        }
        else
        {
            $current_post['ID'] = $current_post_id;
            $current_post['post_status'] = 'private';
            //echo "Success, Going Private";
            wp_update_post($current_post); //Updating the Post Status;*/
        }
    }
    return " ";
}

//Displaying the Interface
function date_to_expire()
{
    global $post;

    if (get_post_type($post) == 'post')
    {

        $date_to_time_today=time();
        $time_to_advance=90*24*60*60; //90 days or 3 months
        $default_time=$date_to_time_today+$time_to_advance; // finding the 3 months advanced time;
        $default_date= date('Y-m-d', $default_time); // converting back to Date again.


        echo '<div class="misc-pub-section misc-pub-section-last" style="border-top: 1px solid #eee;">';
        wp_nonce_field( plugin_basename(__FILE__), 'date_to_expire_nonce' );
        $val = get_post_meta( $post->ID, '_date_to_expire', true ) ? get_post_meta( $post->ID, '_date_to_expire', true ) : $default_date;
        echo '<label for="date_to_expire" class="select-it">Expire Date: </label> <input type="text" size="8" name="date_to_expire" id="date_to_expire" value="'.$val.'" /> <br />';
        echo '</div>';
    }
}


function save_date_to_expire($post_id) {

    if (!isset($_POST['post_type']) )
        return $post_id;

    if ( !wp_verify_nonce( $_POST['date_to_expire_nonce'], plugin_basename(__FILE__) ) )
        return $post_id;

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;

    if ( 'post' == $_POST['post_type'] && !current_user_can( 'edit_post', $post_id ) )
        return $post_id;

    if (!isset($_POST['date_to_expire']))
        return $post_id;
    else {
        $mydata = $_POST['date_to_expire'];
        update_post_meta( $post_id, '_date_to_expire', $_POST['date_to_expire'], get_post_meta( $post_id, '_date_to_expire', true ) );
    }

}